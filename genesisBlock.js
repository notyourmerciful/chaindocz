// JavaScript source code
const crypto = require('crypto');
var fs = require('fs');
var express = require('express');
var app = express();
var wait = require('wait.for');
var bodyParser = require('body-parser');
//Begin user made file includes
var checkTheSum = require('./checkTheSum.js');
var crunchHash = require('./crunchHash.js');

var crunchTwoHash = require('./crunchTwoHash.js');
var checkSum = require('./checkSum.js');



class genesisBlock {
    constructor(name, filePath, emails, description, nameOfNewDoc) {
        
        this.name = name;
        this.filePath = filePath;
        this.emails = emails;
        this.description = description;
        this.nameOfNewDoc = nameOfNewDoc;
        var seed = Math.random(1, 999999999);
        seed = "" + seed;
        crunchHash.setInput(seed);
        debugger;
        this.rollingHash = crunchHash();
        var temp = this.rollingHash;
        fs.writeFile("./lastKey.json", (temp), function (err) {
            if (err) console.log(err);
        });
        this.secHash = null;

    }
    getNameOfNewDow() {
        return this.nameOfNewDoc;
    }

    getDescription() {
        return this.description;
    }

    getName() {
        return this.name;
    }

    getFilePath() {
        return this.filePath;
    }

    getEmails() {
        return this.emails;
    }

    getKey() {
        return this.rollingHash;
    }

    setKey(value) {
        this.rollingHash = value;
    }

    setPresig(input) {
        this.presig = input;
    }

    setRollingHash(input) {
        this.rollingHash = input;
    }
    getSecHash() {
        return this.secHash;
    }


    setSecHash(input) {
        this.secHash = input;
    }
    rollHash(input, input2) {
        if (input2) {
            this.rollingHash = crunchTwoHash(input, input2);
        }
        else {
            this.rollingHash = crunchHash(input);
        }
    }
    finalize() {
        var genBlock = this;
        var filePath = this.filePath;
        var rollingHash = this.rollingHash;
        return new Promise(function (resolve, reject) {
            var checkSumPromise = checkTheSum(filePath);
            checkSumPromise.then(function (resolved) {
                genBlock.setRollingHash(crunchTwoHash(rollingHash, resolved));
                genBlock.setSecHash(crunchHash(rollingHash));
                genBlock.setRollingHash(null);
                resolve(genBlock);
            });

        })
    }

};
module.exports = genesisBlock;